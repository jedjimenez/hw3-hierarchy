﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }


        /*displays a pop-up when a button is clicked. Reference: https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/pop-ups */
        async void popUp(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert(" ", "Press Yes for Page 2 Press No for Previous Page", "Yes", "No");
            Console.WriteLine("Answer: " + answer);

            if (answer == true)
            {
                await Navigation.PushAsync(new Page2());
            }
            else
            {
                await Navigation.PopAsync();
            }
        
        }

        //onDisappearing method that changes the image to another when leaivng the page
        protected override void OnDisappearing()
        {
            
            img.Source = "patrick.jpeg";
        }
    }
}