﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
        int counter = 0;
		public Page2 ()
		{
			InitializeComponent ();     
        }

        //goes to the third page when button is clicked
        async void nextPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }

        //change the background color when button is clicked. It switches to 3 colors by using incrementation.
        void changeBackground(object sender, EventArgs e)
        {
            counter++;

            switch (counter)
            {
                case 1:
                    base.BackgroundColor = Color.Bisque;
                    break;
                case 2:
                    base.BackgroundColor = Color.Linen;
                    break;
                case 3:
                    base.BackgroundColor = Color.Orchid;
                    break;
                default:
                    base.BackgroundColor = Color.Lavender;
                    break;
            }
            
        }
        //goes to the root page when button is clicked
        async void rootPage(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        //changes the colors of the text of the buttons when leaving the page
        protected override void OnDisappearing()
        {
            b1.TextColor = Color.Orange;
            b2.TextColor = Color.Red;
            b3.TextColor = Color.LightBlue;
        }

    }
}