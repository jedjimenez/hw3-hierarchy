﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3_hierarchy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
			InitializeComponent ();
		}

        //when the image is clicked, a pop-up appears. (OnAppearing Method)
        async void imageClicked(object sender, EventArgs e)
        {
            bool answer = await DisplayAlert(" ", "This is the end of the road :(", "Go to Root Page", "Go to Previous Page");
            Console.WriteLine("Answer: " + answer);

            if (answer == true)
            {
      
                await Navigation.PopToRootAsync();
            }
            else
            {
                await Navigation.PopAsync();
            }
           
          
           
        }

        //changes the text, text color, and font attributes when leaving the page
        protected override void OnDisappearing()
        {
            n.Text = "Dead End!";
            n.TextColor = Color.Red;
            n.FontAttributes = FontAttributes.Bold;
        }
    }
}