﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace hw3_hierarchy
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //when button is pressed it goes to the first page
        async void firstPage(object sender, EventArgs e)
        {
          await Navigation.PushAsync(new Page1());
        }

        //when button is pressed it goes to the second page
        async void secondPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

        //when button is pressed it goes to the third page
        async void thirdPage(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }

        //changes the colors of the buttons when leaving the page
        protected override void OnDisappearing()
        {
            b1.BackgroundColor = Color.Orange;
            b2.BackgroundColor = Color.Red;
            b3.BackgroundColor = Color.LightBlue;
        }

        //reverts the colors of the buttons back to its original color when going to the page
        protected override void OnAppearing()
        { 
            b1.BackgroundColor = Color.PaleVioletRed;
            b2.BackgroundColor = Color.PaleVioletRed;
            b3.BackgroundColor = Color.PaleVioletRed;
        }
    }
}
